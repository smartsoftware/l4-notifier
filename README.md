# Laravel User Notifications #

![logo3.png](https://bitbucket.org/repo/ybo85g/images/3896452826-logo3.png)

**Features**

* Attached object
* Type's with templates
* Access to Obj propierties and relations on templates
* Callback by type to define wich users must receive the notification

### Installation & configuration ###

With composer:
```bash
    $ composer require smartsoftware/l4-notifier
```
**Publish config file:**
```bash
    $ php artisan view:publish smartsoftware/l4-notifier
```
**Run migrations**
```bash
    $ php artisan migrate --package='smartsoftware/l4-notifier'
```
**Edit config file:**


```
#!php

<?php
return array(
    /**
     * User Model
     */
    'user_model' => 'User',
    /**
     * get_users id's by notification type for batch notifications
     */
    'get_users' => [
        /**
         * @param int     $type    Notification type id
         * @param mixed   $obj     Attached Object
         * @param int     $sender  User id of notification
         */
        '1' => function($type, $obj, $sender) {
            return [11,12]; // User 11 and 12 will recive notifications of type 1
        },
        '2' => function($type, $obj, $sender) {
            $users = User::whereHas('roles', function($q) {
                $q->where('name', 'someRole');
            });
            return $users; // User with role someRole will recive notifications of type 2
        }
    ]
);
```
**Add trait to user model:**


```
#!php
<?php
use Smartsoftware\L4Notifier\NotifiedTrait;

class User extends Eloquent {

        use NotifiedTrait;

    ...
    ...
}
```

**Create a notification type:**

To create a notification we use *l4notifier:createtype* command

    Usage:
        l4notifier:createtype subject [body] [url]

    Arguments:
        subject               Notification Subject.
        body                  Notification Body.
        url                   Notification URL.


    php artisan l4notifier:createtype "#{obj.id} Ticket closed" "The ticket #{obj.id} was closed by {from.name}" "http://tickets.com/{obj.id}"

The avaiable object for templates are:

    obj:  the attached object (eloquent)
    from: sender user
    to:   receiver user

    You could access to obj propierties like this
    {from.role.id}


## Use ##

### Sending notifications ###

To send a notification to a single user
```
#!php
<?php
    $ticket = Ticket::find(10);
    $user->newNotification()
         ->withType(1)
         ->regarding($ticket)
         ->from($sender_user)
         ->deliver();

```

Send a notification to multiple users:
```
#!php
<?php
use Smartsoftware\L4Notifier\BatchNotification;

    $ticket = Ticket::find(10);
    $users = [1,4,5,110,22];
    BatchNotification::notify($users, 1, $obj, $sender_user->id);

```

### Retrieving notifications ###

Get current user notificatons
```
#!php
<?php
    $user = Auth::user();
    $notifications = $user->notifications()
                          ->unread()
                          ->with('sender')
                          ->orderBy('sent_at','desc');
         
```