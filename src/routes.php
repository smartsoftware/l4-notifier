<?php
Route::group(array('prefix' => 'api'), function() {
    Route::get('notifications/', 'Smartsoftware\L4Notifier\NotificationsController@get');
});
