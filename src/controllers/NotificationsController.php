<?php namespace Smartsoftware\L4Notifier;

use Controller;

use Auth;
use Input;

class NotificationsController extends Controller {

    public function get()
    {
        $user = $this->getCurrentUser();

        $notifications = $user->notifications()->with('sender')->orderBy('sent_at','desc');

        $count  = Input::get('count');
        $unread = Input::get('unread');

        if (is_numeric($count) && $count > 0) {
            $collection = $notifications->take($count);
        } else {
            $collection = $notifications->take(50);
        }
    
        if ($unread){
            $collection = $collection->unread();
        }

        $rtn = $collection->get();

        $collection->update(array("is_read" => 1));

        return $rtn;
    }

    protected function getCurrentUser()
    {
        return Auth::user();
    }
}