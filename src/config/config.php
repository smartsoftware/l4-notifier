<?php
return array(
    /**
     * User Model
     */
    'user_model' => 'User',
    /**
     * get_users id's by notification type
     */
    'get_users' => [
        /**
         * @param int     $type    Notification type id
         * @param mixed   $obj     Attached Object
         * @param int     $sender  User id of notification
         */
        '1' => function($type, $obj, $sender) {
            return [11,12]; // User 11 and 12 will recive notifications of type 1
        }
    ]
);