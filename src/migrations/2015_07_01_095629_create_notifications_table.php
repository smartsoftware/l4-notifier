<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications_type', function(Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('subject', 128)->nullable();
            $table->text('body')->nullable();
            $table->string('url', 256)->nullable();
        });

        Schema::create('notifications', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned();

            $table->integer('notification_type_id')->unsigned();
            $table->foreign('notification_type_id')->references('id')->on('notifications_type');

            $table->integer('object_id')->unsigned();
            $table->string('object_type', 128);

            $table->boolean('is_read')->default(0);
            $table->dateTime('sent_at')->nullable();
            $table->integer('sender_id')->nullable()->unsigned();

            $table->index('user_id');
            $table->index('sender_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notifications');
    }

}
