<?php

namespace Smartsoftware\L4Notifier;

use DB;
use Carbon\Carbon;
use Config;
use Doctrine\Common\Proxy\Exception\InvalidArgumentException;
use Illuminate\Database\Eloquent\Collection;

class BatchNotification {

    /**
     * @param array|Collection $users      array of user's id or eloquent collection
     * @param int              $type       id of notification type
     * @param mixed            $obj        attached obj
     * @param int              $sender     id of sender user
     * @@return boolean
     */
    public static function notify($users, $type, $obj = null, $sender = null)
    {
        if (!is_array($users)) {
            if ($users instanceof Collection) {
                $users = $users->lists('id');
            } else {
                throw new InvalidArgumentException('$users debe ser un array o collecion');
            }
        }

        $data = [];

        $date = new Carbon;

        $obj_type = null;
        $obj_id   = null;

        if ($obj) {
            $obj_type = get_class($obj);
            $obj_id   = $obj->id;
        }

        foreach($users as $id) {
            $data[] = [
                'user_id'              => $id,
                'notification_type_id' => $type,
                'sent_at'              => $date,
                'object_type'          => $obj_type,
                'object_id'            => $obj_id,
                'sender_id'            => $sender
            ];
        }

        return DB::table('notifications')->insert($data);
    }

    /**
     * Notify all users given by config files callback get_users=[type]
     *
     * @param int              $type       id of notification type
     * @param mixed            $obj        attached obj
     * @param int              $sender     id of sender user
     * @@return boolean
     */
    public static function notifyByType($type, $obj = null, $sender = null)
    {
        $users = self::getUserByType($type, $obj, $sender);

        if (is_array($users) && empty($users)) {
            return false;
        }

        if (count($users) == 0) return false;

        return self::notify($users, $type, $obj, $sender);
    }

    /**
     * Retorna los usuarios a los que se les debe notificar un tipo dado
     *
     * @param $type
     * @return array
     */
    protected static function getUserByType($type, $obj = null, $sender = null)
    {
        $fn = Config::get('l4-notifier::get_users.'.$type);

        if (!$fn) return [];

        return $fn($type, $obj, $sender);
    }
}