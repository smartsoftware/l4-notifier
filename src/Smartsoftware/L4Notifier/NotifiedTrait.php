<?php namespace Smartsoftware\L4Notifier;

use Smartsoftware\L4Notifier\Notification;

trait NotifiedTrait {

    public function notifications()
    {
        return $this->hasMany('Smartsoftware\L4Notifier\Notification');
    }

    public function newNotification()
    {
        $notification = new Notification;
        $notification->user()->associate($this);

        return $notification;
    }
}