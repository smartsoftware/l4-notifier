<?php namespace Smartsoftware\L4Notifier;

use \Illuminate\Database\Eloquent\Model as Eloquent;

class NotificationType extends Eloquent
{
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notifications_type';

    protected $fillable   = ['subject', 'body', 'url'];

}