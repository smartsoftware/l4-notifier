<?php namespace Smartsoftware\L4Notifier;

use Illuminate\Support\ServiceProvider;
use Smartsoftware\L4Notifier\CreatetypeCommand;

/**
 * Class L4NotifierServiceProvider
 * @package Smartsoftware\L4Notifier
 */
class L4NotifierServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('smartsoftware/l4-notifier');
        include __DIR__.'/../../routes.php';

        $this->commands(array(
            'command.l4notifier.createtype'
        ));

        $this->registerCommands();
    }


    /**
     * Register the artisan commands.
     *
     * @return void
     */
    private function registerCommands()
    {
        $this->app->bindShared('command.l4notifier.createtype', function ($app) {
            return new CreatetypeCommand();
        });
    }

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

    /**
     * Get the services provided.
     *
     * @return array
     */
    public function provides()
    {
        return array(
            'command.l4notifier.createtype'
        );
    }

}
