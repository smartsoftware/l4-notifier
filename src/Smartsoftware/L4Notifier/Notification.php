<?php namespace Smartsoftware\L4Notifier;

use \Illuminate\Database\Eloquent\Model as Eloquent;
use Config;
use Carbon\Carbon;

class Notification extends Eloquent
{
    /**
     * Regex to get values between curly brachet {$value}
     */
    const RULE = '/\{(.+?)(?:\{(.+)\})?\}/';

    protected $fillable = ['sender_id' ,'user_id', 'notification_type_id', 'object_id', 'object_type', 'sent_at'];

    protected $appends = ['body','subject','url'];

    private $relatedObject = null;

    public $timestamps = false;

    public function getDates()
    {
        return ['sent_at'];
    }

    public function user()
    {
        $model = Config::get('l4-notifier::user_model');
        return $this->belongsTo($model, 'user_id');
    }

    public function type()
    {
        return $this->belongsTo('Smartsoftware\L4Notifier\NotificationType', 'notification_type_id');
    }

    public function sender()
    {
        $model = Config::get('l4-notifier::user_model');
        return $this->belongsTo($model, 'sender_id');
    }

    public function from($user)
    {
        $this->sender()->associate($user);

        return $this;
    }

    public function to($user)
    {
        $this->user()->associate($user);

        return $this;
    }

    public function scopeUnread($query)
    {
        return $query->where('is_read', '=', 0);
    }

    public function scopeType($query, $t) {
        return $query->where('notification_type_id', $t);
    }

    public function withType($type)
    {
        $this->notification_type_id = $type;

        return $this;
    }

    public function  withUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    public function regarding($object)
    {
        if(is_object($object))
        {
            $this->object_id   = $object->id;
            $this->object_type = get_class($object);
        }

        return $this;
    }

    public function deliver()
    {
        $this->sent_at = new Carbon;
        $this->save();

        return $this;
    }

    public function hasValidObject()
    {
        try
        {
            $object = call_user_func_array($this->object_type . '::findOrFail', [$this->object_id]);
        }
        catch(\Exception $e)
        {
            return false;
        }

        $this->relatedObject = $object;

        return true;
    }

    public function getObject()
    {
        if($this->object_type)
        {
            $hasObject = $this->hasValidObject();

            if(!$hasObject)
            {
                throw new \Exception(sprintf("No valid object (%s with ID %s) associated with this notification.", $this->object_type, $this->object_id));
            }
        }

        return $this->relatedObject;
    }

    public function getBodyAttribute()
    {
        $type = $this->type;

        return $this->renderString($type->body);
    }

    public function getSubjectAttribute()
    {
        $type = $this->type;

        return $this->renderString($type->subject);
    }

    public function getUrlAttribute()
    {
        $type = $this->type;

        return $this->renderString($type->url);
    }

    protected function renderString($string)
    {
        $specialValues = $this->getValues($string);

        if ($specialValues > 0) {
            foreach($specialValues as $value) {
                $string = str_replace('{'.$value.'}', $this->getSpecialValue($value), $string);
            }
        }

        return $string;
    }

    protected function getSpecialValue($v)
    {
        $r = explode('.', $v);

        $obj = null;

        switch($r[0]) {
            case 'url':
                $obj = $this->url;
                break;
            case 'to':
                $obj = $this->user;
                break;
            case 'from':
                $obj = $this->sender;
                break;
            case 'obj':
                $obj = $this->getObject();
                break;
        }

        if (!$obj) return null;

        while ( $par = next($r) ) {
            if ($obj->$par) {
                $obj = $obj->$par;
            } else {
                return null;
            }
        };

        return $obj;
    }

    /**
     * Get the values between {}
     * and return an array of it
     *
     * @param $body
     * @return mixed
     */
    protected function getValues($body)
    {
        $values = [];
        preg_match_all(self::RULE, $body, $values);
        return $values[1];
    }

}