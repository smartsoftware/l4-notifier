<?php namespace Smartsoftware\L4Notifier;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

use Smartsoftware\L4Notifier\NotificationType;

class CreatetypeCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'l4notifier:createtype';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a new notification type.';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $body    = $this->argument('body');
        $subject = $this->argument('subject');
        $url     = $this->argument('url');

        $this->line('');
        $this->info( "Notification: $subject" );
        $this->info( $body );
        $this->comment( $url );
        $this->line('');

        if ( $this->confirm("Proceed with the notification type creation? [Yes|no]") ) {

            $this->line('');

            $this->info( "Creating notification type..." );
            if ( $this->createType($subject, $body, $url) ) {

                $this->info( "Notification type successfully created!" );
            } else {
                $this->error(
                    "Coudn't create notification type.\n"
                );
            }
            $this->line('');

        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('subject', InputArgument::REQUIRED, 'Notification Subject.'),
            array('body', InputArgument::OPTIONAL, 'Notification Body.', null),
            array('url', InputArgument::OPTIONAL, 'Notification URL.', null),
        );
    }

    /**
     * Create the migration.
     *
     * @param string $subject
     * @param string $body
     *
     * @return bool
     */
    protected function createType($subject, $body, $url)
    {
        $type = new NotificationType;

        $type->body = $body;
        $type->subject = $subject;
        $type->url     = $url;

        return $type->save();
    }
}
